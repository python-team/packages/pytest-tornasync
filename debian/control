Source: pytest-tornasync
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Joseph Nahmias <jello@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
 python3-pytest <!nocheck>,
 python3-tornado <!nocheck>,
Standards-Version: 4.6.0
Homepage: https://github.com/eukaryote/pytest-tornasync
Vcs-Browser: https://salsa.debian.org/python-team/packages/pytest-tornasync
Vcs-Git: https://salsa.debian.org/python-team/packages/pytest-tornasync.git
Rules-Requires-Root: no

Package: python3-pytest-tornasync
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: pytest plugin for testing Tornado apps using native coroutine tests
 A simple pytest plugin that provides some helpful fixtures for testing Tornado
 (version 5.0 or newer) apps and easy handling of plain (undecoratored) native
 coroutine tests (Python 3.5+).
 .
 Why another Tornado pytest plugin when the excellent pytest-tornado already
 exists? The main reason is that I didn't want to have to decorate every test
 coroutine with @pytest.mark.gen_test. This plugin doesn't have anything like
 gen_test. Defining a test with async def and a name that begins with test_ is
 all that is required.
